import React from "react";
import Player from "./Components/Player";

const Jankenpon = () => {
  return (
    <div className="jankenpon">
      <header className="jankenpon-header">
        <h1 className="title">JANKENPON</h1>
        <Player />
      </header>
    </div>
  );
};

export default Jankenpon;
