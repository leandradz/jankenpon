import { FaHandRock, FaHandPaper, FaHandPeace } from "react-icons/fa";

const Scoreboard = ({ player, machine }) => {
  return (
    /* ESCOLHA DO USUARIO E RANDOM MACHINE */
    <>
      <div className="current-play">
        <div className="machine">
          {machine === "Pedra" ? (
            <FaHandRock className="icon" />
          ) : machine === "Papel" ? (
            <FaHandPaper className="icon" />
          ) : machine === "Tesoura" ? (
            <FaHandPeace className="icon" />
          ) : (
            ""
          )}
        </div>
        <div className="player">
          {player === "Pedra" ? (
            <FaHandRock className="icon" />
          ) : player === "Papel" ? (
            <FaHandPaper className="icon" />
          ) : player === "Tesoura" ? (
            <FaHandPeace className="icon" />
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
};
export default Scoreboard;
