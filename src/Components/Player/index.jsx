import { useState, useEffect } from "react";
import Scoreboard from "../Scoreboard";
import { FaHandRock, FaHandPaper, FaHandPeace } from "react-icons/fa";

const Player = () => {
  const [currentPlay, setCurrentPlay] = useState([]);
  const [currentMachine, setCurrentMachine] = useState([]);
  const [countVictoryPlayer, setCountVictoryPlayer] = useState(0);
  const [countVictoryMachine, setCountVictoryMachine] = useState(0);
  const [isNewPlay, setisNewPlay] = useState(true);

  //Pegando a jogada atual do Player e Machine
  const plays = (itemSelecionado) => {
    const arrJankenpon = ["Pedra", "Papel", "Tesoura"];
    setCurrentMachine(
      arrJankenpon[Math.floor(Math.random() * arrJankenpon.length)]
    );
    setCurrentPlay(itemSelecionado);
    setisNewPlay((isNewPlay) => !isNewPlay);
  };

  useEffect(() => {
    //validação de vitória
    if (currentPlay === currentMachine) {
      console.log("empate");
    } else if (currentPlay === "Pedra") {
      if (currentMachine === "Tesoura") {
        setCountVictoryPlayer(countVictoryPlayer + 1);
      } else {
        setCountVictoryMachine(countVictoryMachine + 1);
      }
    } else if (currentPlay === "Tesoura") {
      if (currentMachine === "Papel") {
        setCountVictoryPlayer(countVictoryPlayer + 1);
      } else {
        setCountVictoryMachine(countVictoryMachine + 1);
      }
    } else if (currentPlay === "Papel") {
      if (currentMachine === "Pedra") {
        setCountVictoryPlayer(countVictoryPlayer + 1);
      } else {
        setCountVictoryMachine(countVictoryMachine + 1);
      }
    }
  }, [isNewPlay]);

  return (
    //Placar
    <div className="container">
      <div className="scoreboard">
        <div className="placar">
          <h1>Computador</h1>
          <span>{countVictoryMachine}</span>
        </div>
        x
        <div className="placar">
          <h1>Player</h1>
          <span>{countVictoryPlayer}</span>
        </div>
      </div>

      {/* Jogada atual do usuário e máquina */}
      <Scoreboard player={currentPlay} machine={currentMachine} />

      {/* Botões captando escolha do usuário */}
      <div className="container-button">
        <button onClick={() => plays("Pedra")}>
          <FaHandRock className="icon-button" />
        </button>
        <button onClick={() => plays("Papel")}>
          <FaHandPaper className="icon-button" />
        </button>
        <button onClick={() => plays("Tesoura")}>
          <FaHandPeace className="icon-button" />
        </button>
      </div>
    </div>
  );
};
export default Player;
